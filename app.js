// access the dom elements must change 
const computersItems = document.getElementById('computers');
const features = document.getElementById('screen');
const salary = document.getElementById('salaryPerHour')
const currentBalance = document.getElementById('currentBalance')
const computerName = document.getElementById("computerName")
const computerInfo = document.getElementById("computerInfo")
const computerPrice = document.getElementById("computerPrice")
const currentLoan = document.getElementById("currentLoan")
const specs = document.getElementById('info')

// this function display the clear button and loan text when needed
function displayBtn() {
   document.querySelector(".btnClearLoan").style.display = "inline";
   document.querySelector("#loanLb").style.display = "inline";
   document.querySelector("#currentLoan").style.display = "inline";
   document.querySelector("#currentLoanSpan").style.display = "inline";
}


// this function hide the clear button and loan text
function hideDisplayBtn() {
   document.querySelector(".btnClearLoan").style.display = "none";
   document.querySelector("#loanLb").style.display = "none";
   document.querySelector("#currentLoan").style.display = "none";
   document.querySelector("#currentLoanSpan").style.display = "none";
}

let computersCollections = [];
let currentSalary = 0;


// fetch data from API
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
   .then(res => res.json())
   .then(data => computersCollections = data)
   .then(computers => addComputersToMenu(computers));
   const addComputersToMenu = (_computers) => {
   _computers.forEach(x => addComputerToMenu(x));


   // add Item to the menu initial state
}
const addComputerToMenu = (computer) => {
   const computerElement = document.createElement('option');
   computerElement.value = computer.id;
   computerElement.appendChild(document.createTextNode(computer.title));
   computersItems.appendChild(computerElement);
   computerName.innerHTML = computersCollections[0].title;
   computerInfo.innerHTML = computersCollections[0].description;
   computerPrice.innerHTML = computersCollections[0].price + " " + "kr";
   let imgLink = "https://noroff-komputer-store-api.herokuapp.com/" + computersCollections[0].image;;
   image["src"] = imgLink;
   const selectedComputer = computersCollections[0];
   let info = selectedComputer.specs;
   var str = '<ul>'
   info.forEach(function (slide) {
      str += '<li>' + slide + '</li>';
   });
   str += '</ul>';
   document.getElementById("info").innerHTML = str;
}

//populate fields computers info
const handleComputerType = e => {
   const selectedComputer = computersCollections[e.target.selectedIndex];
   let info = selectedComputer.specs;
   var str = '<ul>'
   info.forEach(function (slide) {
   str += '<li>' + slide + '</li>';});
   str += '</ul>';
   document.getElementById("info").innerHTML = str;
   let imgLink = "https://noroff-komputer-store-api.herokuapp.com/" + selectedComputer.image;;
   image["src"] = imgLink;

   document.querySelector(".btnBuyNow").style.display = "inline";
   computerName.innerHTML = selectedComputer.title;
   computerInfo.innerHTML = selectedComputer.description;
   computerPrice.innerHTML = selectedComputer.price + " " + "kr";
}


// select the computer from menu
computersItems.addEventListener("change", handleComputerType);


//this function add to the salary by clicking work buttons
function addSalary() {
   salary.innerHTML = currentSalary += 100
}


// adding salary to the bank and pay 10% if there is an active loan
function addSalaryToTheBank() {
   const cur_Loan = parseInt(currentLoan.innerHTML)
   // her we take 10% loan to pay our loan
   if (cur_Loan > 0) {
      const tenPercentOfSalary = 0.1 * parseInt(salary.innerHTML)
      // if 10% salary is more than amount of loan then minus the loan and put the rest in the bank
      if (tenPercentOfSalary > cur_Loan) {
         const outstandingBalance = tenPercentOfSalary - cur_Loan;
         window.alert("your loan will be  0 and this amount " + outstandingBalance + " will be added to your accout")
         // document.querySelector(".btnClearLoan").style.display="none";
         hideDisplayBtn();
         let existingAmountToInt = parseInt(currentBalance.innerHTML)
         currentBalance.innerHTML = existingAmountToInt += currentSalary - tenPercentOfSalary + tenPercentOfSalary - cur_Loan;
         currentLoan.innerHTML = cur_Loan - tenPercentOfSalary + tenPercentOfSalary - cur_Loan;
         EmptyexistingSalary();
      }
      else {
         let existingAmountToInt = parseInt(currentBalance.innerHTML)
         currentBalance.innerHTML = existingAmountToInt += currentSalary - tenPercentOfSalary
         currentLoan.innerHTML = cur_Loan - tenPercentOfSalary;
         EmptyexistingSalary();
      }
   }
   else {
      let existingAmount = document.getElementById("currentBalance").innerHTML;
      let existingAmountToInt = parseInt(existingAmount)
      currentBalance.innerHTML = existingAmountToInt += currentSalary
      EmptyexistingSalary();
   }
}


// clean the salary after transfer to the bank
function EmptyexistingSalary() {
   salary.innerHTML = currentSalary = 0;
}


//this function checks if you have enough money to buy or you should get loan 
function buyNew() {
   const com_Price = parseInt(computerPrice.innerHTML)
   const cur_Balance = parseInt(currentBalance.innerHTML)
   if (cur_Balance >= com_Price) {
      // balance - computer price if you dont have enough money the you can get a loan
      currentBalance.innerHTML = cur_Balance - com_Price;
      window.alert("Your order is on the way")
   }
   else {
      window.alert("you dont have enough money please get some loan")
   }
}

// apply for loan if you buy something more than your balance
function getLoan() {
   const checkIfNull = parseInt(currentLoan.innerHTML);
   // here we check if you already have a loan or not 
   if (checkIfNull == 0) {
      const prompt_Amount = getInteger();
      let existingAmount = document.getElementById("currentBalance").innerHTML;
      let existingAmountToInt = parseInt(existingAmount)
      // check is the amount is more than dubble
      if (existingAmountToInt * 2 < prompt_Amount) {
         window.alert("you can only loan upto 2 times of your  bank balance")
      }
      else {
         currentLoan.innerHTML = prompt_Amount;
         currentBalance.innerHTML = existingAmountToInt += prompt_Amount;
         // hide the button if the loan is zero
         if (parseInt(currentLoan.innerHTML) > 0) {
            displayBtn();
         }
      }
   } else {
      window.alert("you already have a loan")
   }
}


// clear the loan 
function clearLoan() {
   if (currentLoan.innerHTML > 0) {
      currentBalance.innerHTML = currentBalance.innerHTML - currentLoan.innerHTML;
      currentLoan.innerHTML = 0;
      // document.querySelector(".btnClearLoan").style.display="none";
      hideDisplayBtn();

   }
}


// validate prompt window for loan
function getInteger() {
   while (true) {
      let input = prompt("Input number: ");
      if (input == null) {
         // user hit cancel
         alert("I'm out of here.")
         return 0;
      } else {
         if (input.length <= 0 || isNaN(input) || input.indexOf(' ') >= 0)  {
            // user pressed OK, but input invalid or does not input anything
            alert("Invalid input.");
         } else {
            // user typed something valid and hit OK
            return parseInt(input);
         }
      }
   }
}




